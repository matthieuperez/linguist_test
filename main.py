"""How to have translations ?

The goal is to be able to generate & import the module linguist_rc.

With it, the self.tr() functions will have translations depending on the QLocale chosen.
By default QLocale.system() is chosen. It can be overriden for test by setting the LANGUAGE environment variable.
Example: LANGUAGE=de python main.py

To generate this linguist_rc.py file, one need a linguist.qrc file and use "pyside6-rcc linguist.qrc -o linguist_rc.py"
This is a resource file (just like the one for icons) and it needs to be "compiled" to python.
It contains a list of binary trasnlation files *.qm. For example here translations/example_de.qm
This file was created by: pyside6-lrelease example_de.ts -qm translations/example_de.qm

The example_de.ts file is used by Qt Linguist. To generate (or update) it from the source files:
pyside6-lupdate main.py -ts example_de.ts

it will look for all source files given (here only main.py) and extract the strings to be translated.

So in fact, for pyside6, first:
have sources with strings to translates
pyside6-lupdate main.py -ts example_de.ts
pyside6-lrelease example_de.ts -qm translations/example_de.qm
pyside6-rcc linguist.qrc -o linguist_rc.py
LANGUAGE=de python main.py

for pyqt6, sadly I can't find how to perform the last step, so, I still use PySide6 for it, and then replace
the import:
pylupdate6 main.py -ts example_de.ts
lrelease example_de.ts -qm translations/example_de.qm
pyside6-rcc linguist.qrc | sed '0,/PySide6/s//PyQt6/' > linguist_rc.py
LANGUAGE=de python main.py


Actually the last part is only used if we want to keep the ":translations" path instead of a ".translations" path.
I need to check how it works with an installed package. I guess the .py is bundled automatically even with pyinstaller.
Which helps. Then just import linguist_rc here and the translations.
This is what I do with alphaMIC & icons, I do use the :icons for example & it works
"""

# Copyright (C) 2022 The Qt Company Ltd.
# SPDX-License-Identifier: LicenseRef-Qt-Commercial OR BSD-3-Clause

# Choose between pyside6 or pyqt6 (not necessary)
# import os

# os.environ["QT_API"] = "pyside6"
# os.environ["QT_API"] = "pyqt6"


import sys

from qtpy.QtCore import QItemSelection, QLibraryInfo, QLocale, QTranslator, Slot
from qtpy.QtWidgets import (
    QAbstractItemView,
    QApplication,
    QListWidget,
    QMainWindow,
)

# import linguist_rc  # necessary to use the ":translations" notation, otherwise need a "." and note sure how it works with packages
from linguist_rc import qInitResources as init_translations


class Window(QMainWindow):
    def __init__(self):
        super().__init__()
        file_menu = self.menuBar().addMenu(self.tr("&File"))
        quit_action = file_menu.addAction(self.tr("Quit"))
        quit_action.setShortcut(self.tr("CTRL+Q"))
        quit_action.triggered.connect(self.close)
        help_menu = self.menuBar().addMenu(self.tr("&Help"))
        about_qt_action = help_menu.addAction(self.tr("About Qt"))
        about_qt_action.triggered.connect(QApplication.aboutQt)

        self._list_widget = QListWidget()
        self._list_widget.setSelectionMode(QAbstractItemView.MultiSelection)
        self._list_widget.selectionModel().selectionChanged.connect(
            self.selection_changed
        )
        self._list_widget.addItem("C++")
        self._list_widget.addItem("Java")
        self._list_widget.addItem("Python")
        self.setCentralWidget(self._list_widget)

    @Slot(QItemSelection, QItemSelection)
    def selection_changed(self, selected, deselected):
        count = len(self._list_widget.selectionModel().selectedRows())
        message = self.tr("%n language(s) selected", "", count)
        my_added_test = self.tr("coucou")
        self.statusBar().showMessage(message + " ; " + my_added_test)


if __name__ == "__main__":
    print(QLocale.system())  # force with LANGUAGE=de python main.py
    init_translations()
    app = QApplication(sys.argv)

    path = QLibraryInfo.path(QLibraryInfo.TranslationsPath)
    translator = QTranslator(app)
    if translator.load(QLocale.system(), "qtbase", "_", path):
        app.installTranslator(translator)
    translator = QTranslator(app)
    path = ":/translations"
    if translator.load(QLocale.system(), "example", "_", path):
        app.installTranslator(translator)

    window = Window()
    window.show()
    sys.exit(app.exec())
