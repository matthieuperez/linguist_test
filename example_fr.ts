<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en">
<context>
    <name>Window</name>
    <message>
        <location filename="main.py" line="67"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="main.py" line="68"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="main.py" line="69"/>
        <source>CTRL+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="main.py" line="71"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="main.py" line="72"/>
        <source>About Qt</source>
        <translation>Au sujet de Qt</translation>
    </message>
    <message numerus="yes">
        <location filename="main.py" line="88"/>
        <source>%n language(s) selected</source>
        <translation>
            <numerusform>%n langage sélectionné</numerusform>
            <numerusform>%n langages sélectionnés</numerusform>
        </translation>
    </message>
    <message>
        <location filename="main.py" line="89"/>
        <source>coucou</source>
        <translation>COUCOU</translation>
    </message>
</context>
</TS>
